
- If you use an arduino clone, chances are that you'll need configure the USB drivers.
  In Linux, the .... driver is already installed.

- If the non-arduino branded board is not detected, check if blrtty is not claiming /dev/USB*.
  If so, and you don't need blrtty, uninstall it.

