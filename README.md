# RTeg - Real-Time by example

RTeg (Real-Time _exempli gratia_) is a collection of code examples and programming exercises intended for illustration of general concepts and techniques concerning real-time computer systems. It may be useful as a companion resource for students and instructors exploring this field.

Copyright (c) 2019-2023 Monaco F.J. - Disributed under GNU GPL vr. 3
(see enclosed file COPPYING for further information)