/* #include "pt/pt.h" */
#include <stdio.h>

typedef unsigned short lc_t;

struct pt {
  lc_t lc;
};

static int counter;
static struct pt example_pt;

static
char example(struct pt *pt)
{
  switch((pt)->lc) { case 0: 

      printf ("This is done\n");
      
      while(1){

	(pt)->lc = __LINE__; case __LINE__: 
	{
	  printf ("counter %d\n", counter);
	
	  if(!(counter == 20)) return 0;
	
	  printf("Threshold reached (%d)\n", counter);
	  counter = 0;
	}
	
	(pt)->lc = __LINE__; case __LINE__: 
	{
	
	  if(!(counter == 15)) return 0;
	
	  printf("Threshold reached (%d)\n", counter);
	  counter = 0;
	}

      }
      (pt)->lc = 0;;
    return 3;
  }
}

int main(void)
{
  counter = 0;
  (&example_pt)->lc = 0;;

  while(1) {
    example(&example_pt);
    counter++;
  }
  return 0;
}
