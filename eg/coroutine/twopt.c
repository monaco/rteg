#include <stdio.h>
#include <unistd.h>
#include "pt/pt.h"

static int go1= 0, go2=0;
static struct pt f1_pt, f2_pt;

static
PT_THREAD ( f1(struct pt *pt) )
{
  PT_BEGIN (pt);

  printf ("f1 started\n");
  while (1)
    {
      PT_WAIT_UNTIL (pt, go1);
      printf ("f1\n");
      go1 = 0;
    }

  PT_END (pt);
}

static
PT_THREAD ( f2(struct pt *pt) )
{
  PT_BEGIN (pt);

  printf ("f2 started\n");
  while (1)
    {
      PT_WAIT_UNTIL (pt, go2);
      printf ("f2\n");
      go2 = 0;
    }

  PT_END (pt);
}

int main ()
{
  
  PT_INIT(&f1_pt);

  while (1)
    {
      f1(&f1_pt);
      f2(&f2_pt);
      sleep (1);
      go1 = 1;
      go2 = 1;
    }
  
  return 0;
}
