#include <stdio.h>
#include <stdlib.h>

#define STEP_MAX 8

int step;

void task_0(), task_1();

void (*tasks[])()  = {task_0, task_1, NULL}; 

int main()
{
  int i;
  step = 0;
  while (1)
    {
      i=0;
      while (tasks[i])
	tasks[i++]();
      step = (step + 1) % STEP_MAX;
    }
}


void task_0()
{
  switch (step)
    {
    case 0:
      printf ("task 0 : step  0\n");
      break;
    case 1:
      printf ("task 0 : step  1\n");
      break;
    case 2:
      printf ("task 0 : step  2\n");
      break;
    case 3:
      printf ("task 0 : step  3\n");
      break;
    case 4:
      printf ("task 0 : step  4\n");
      break;
    default:
      /* step = (step > 7) ? 0 : step; */
      printf ("task 0 : step %2d *\n", step);
      break;
    }
  /* current_task = (current_task + 1) % TASK_MAX; */
  /* current_task++; */
}

void task_1()
{
  switch (step)
    {
    case 0:
      printf ("task 1 : step  0\n");
      break;
    case 1:
      printf ("task 1 : step  1\n");
      break;
    case 2:
      printf ("task 1 : step  2\n");
      break;
    case 3:
      printf ("task 1 : step  3\n");
      break;
    case 4:
      printf ("task 1 : step  4\n");
      break;
    default:
      /* step = (step > 7) ? 0 : step; */
      printf ("task 1 : step %2d *\n", step);
      break;
    }
  /* current_task = (current_task + 1) % TASK_MAX; */
  /* current_task++; */
}
