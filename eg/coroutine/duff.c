#include <stdio.h>

/* Function foo() is a coroutine: each time it's invoked, the function returns
   a different value: first is returns a, then a+1, a+2... until it returns b.
   (and repeats the sequence from that point onwards. 

   The function is implemented using Duff's Device.   

   Weirdy, this is a legal code according to C specification (although
   some compilers may not understand it; GCC does).
*/

int foo(int a, int b)
{
  static int i;			/* This variable needs to be static. */
  static int state = 0;		/* This variable needs to be static. */

  switch (state)
    {
      
    case 0:			/* When state is 0, the loop is started. */
      state = 1;
      for (i=a ; i<=b; i++)
	{
	  return i;
      
	case 1:			/* When state is 1, the switch statement leads */
	  ;                     /* to this point. Notice that the 'case' is */
	}			/* within the loop block: after landing here, */
    }				/* execution jumps back to the 'for' statement.*/
  
  state = 0;			/* Only when the loop is done we get here. */
  return 0;
}


int main ()
{
  int i;

  while (  i = foo(1, 5)  )	/* Call foo() several times. */
    printf ("foo: %d\n", i);
}
