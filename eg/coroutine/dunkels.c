#include <stdio.h>
#include <unistd.h>
#include "pt/pt.h"
 
static int counter;
static struct pt example_pt;
 
static
PT_THREAD(example(struct pt *pt))
{
  PT_BEGIN(pt);
  
  while(1) {
    PT_WAIT_UNTIL(pt, counter == 5);
    printf("Threshold reached (%d)\n", counter);
    counter = 0;
  }
  
  PT_END(pt);
}
 
int main(void)
{
  counter = 0;
  PT_INIT(&example_pt);
  
  while(1) {
    printf ("count %d\n", counter);
    example(&example_pt);
    counter++;
    sleep (1);
  }
  return 0;
}
