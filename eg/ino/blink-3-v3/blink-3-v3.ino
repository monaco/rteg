#include "timer-interrupt.h"   // Timer interrupt library.

#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 
#define LED3    6  // pin of the LED 2 
#define BUTTON  2  // opin of the push button

#define R     100 // Interrupt time resolution (ms)

int led1_state = LOW;     // Whether LED 1 is on or off  
int led2_state = LOW;     // Whether LED 2 is on or off 
int led3_state = LOW;     // Whether LED 3 is on or off 

void setup()
{

  pinMode (LED1, OUTPUT);	
  pinMode (LED2, OUTPUT);	
  pinMode (LED3, OUTPUT);	
  pinMode (BUTTON, INPUT);

  // Set the timer interval. It will cause the timer to trigger
  // every 100ms and call the user-defined interrupt service routine
  // timer1_action().

  timer1_interval(100);  

  // Configure the interrupt service routine for the push button.
  // led3_toggle() will be called when signal in the button's pin is
  // rising (low-to-high border-triggered).

  attachInterrupt(digitalPinToInterrupt(BUTTON), led3_toggle, RISING);

  Serial.begin(9600);
}

// We don't use the main loop for real-time tasks anymore.
// It may serve non real-time (background) tasks, instead.

int count = 0;

void loop()           
{
  Serial.println(count++);
  delay (1000);               // This delay won't retard real-time tasks.
}


// The interrupt service routine for the timer.

volatile unsigned int led1_count = 0;   
volatile unsigned int led2_count = 0;   

void timer1_action()
{
    timer1_reset();

    if (!led1_count)      
    {
      toggle (LED1, &led1_state);
    }
    led1_count = (led1_count + 1) % 5;   // Count to 5:  500ms

  if (!led2_count)        
    {
      toggle (LED2, &led2_state);
    }
    led2_count = (led2_count + 1) % 4;  // Count to 4: 400ms

}

// Interrupt service routine for the button.

void led3_toggle()
{
  toggle (LED3, &led3_state);
}

void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}
