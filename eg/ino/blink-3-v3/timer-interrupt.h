#ifndef TIMER_INTERRUPT_H
#define TIMER_INTERRUPT_H

int timer1_interval(int time);  // In milisseconds
void timer1_action();            // User-defined function.
void timer1_reset();             // Reset timer 1

#endif