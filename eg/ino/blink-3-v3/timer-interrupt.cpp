#include <Arduino.h>
#include "timer-interrupt.h"

const unsigned int prescalar_available[] = {0, 1, 8, 64, 256, 1024};

// Program timer 1 to trigger every 'time' milliseconds and issue
// the user-defined interrup service routine timer1_action().
// Minimum time interval: 5ms.

int timer1_interval(int time)
{
  unsigned long int prescalar;
  int i;

  cli();     // Disable interrupts

  // Set the Timer/Counter Control Registers of timer 1.

  TCCR1A  = 0;           // Zero all bits of control register 1.     
  TCCR1B  = 0;           // Zero all bits of control register 2.

  //  Compute the prescalar.
  //
  //      Prescalar * 2^16 / 16MHz >= time
  //      Prescalar in A = {0, 1, 8, 64, 256, 1024}
  //      A[i] = Prescalar

  prescalar = 16000000 * time / 65536  / 1000;                     
  for (i=0; (i<5) && (prescalar > prescalar_available[i]); i++); 

  TCCR1B |= i;

  // Zero the timer (counter).

  TCNT1 = 0;             

  // Enable the Compare Interrupt for timer 1.

  TIMSK1 |= B00000010;   // Bits 2 and 3. Timer 1 : 01

  // Set the Output Compare Register for timer1.
  // When timer equals OCRIA1, an interrupt is generated.

  OCR1A = time * 16000000 / prescalar_available[i] / 1000;  

  sei();     // Enable interrupts

  return prescalar_available[i];
}

// The interrup service routine calls the user-defined function
// timer1_action().

ISR(TIMER1_COMPA_vect)    // For timer 1
{
  timer1_action();
}

void timer1_reset()
{
  TCNT1 = 0;
}