
#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 
#define LED3    6  // pin of the LED 2 
#define BUTTON  2  // opin of the push button

#define T     500	// LED blinking period

int button_now;          // Whether the button is pressed or not
int button_before;       // Whether the button was pressed before
int led3_state = LOW;     
int led1_state = LOW;
int led2_state = LOW;

int time_now;
int time_led1 = 0; 
int time_led2 = 0; 

#define LED2_PHASE T/2

void setup()
{
  pinMode (LED1, OUTPUT);	
  pinMode (LED2, OUTPUT);	
  pinMode (LED3, OUTPUT);	
  pinMode (BUTTON, INPUT);
 }

void loop() 
{
  // Get current time.

  time_now = millis(); 

  // If T/2 has elapsed, toggle LED1

  if ( (time_now - time_led1) > T)
  {
    toggle (LED1, &led1_state); 
    time_led1 = time_now;
  }

  // If T/2 has elapsed (phase T/2), toggle LED2;

 if ( (time_now - LED2_PHASE - time_led2) > T)
  {
    toggle (LED2, &led2_state);
    time_led2 = time_now - LED2_PHASE;
  }


  // Detect button change and toggle LED3.

  button_before = button_now;
  button_now = digitalRead(BUTTON);

  if (button_before == LOW && button_now == HIGH)
  {
    toggle (LED3, &led3_state);
  }
}

void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}