#include <Arduino_FreeRTOS.h>

void setup()
{

  Serial.begin(9600);

  // Create task 1
  xTaskCreate(
    do_task_1,         // Task function
    "Task 1",          // Desciptive string
    100,               // Stack depth
    NULL,              // Task argument
    1,                 // Task priority
    NULL               // Task handler
  );

 // Create task 2
  xTaskCreate(
    do_task_2,         // Task function
    "Task 2",          // Desciptive string
    100,               // Stack depth
    NULL,              // Task argument
    1,                 // Task priority
    NULL               // Task handler
  );


}

void do_task_1(void *args)    // Task1's function
{
  while (1)
  {
    Serial.println("Task 1");
  }
}

void do_task_2(void *args)   // Task2's function
{
  while (1)
  {
    Serial.println("Task 2");
  }
}

void loop(){}
