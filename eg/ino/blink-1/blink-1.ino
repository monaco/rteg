
#define LED     8	 // Pin of the (external) LED 
#define T     500	 // LED toggling period (1 Hz blink frequency)

int led_state = 0; 

void setup()
{
  pinMode (LED, OUTPUT);	
}

void loop()
{
  toggle (LED, &led_state);

  delay(T);
}

void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}

