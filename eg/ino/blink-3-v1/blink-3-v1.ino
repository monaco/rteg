
#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 
#define LED3    6  // pin of the LED 3 
#define BUTTON  2  // pin of the push button

#define T     500	// LED blinking period

#define OTHER_LEDS 1

int button_now;          // Whether the button is pressed or not
int button_before;       // Whether the button was pressed before

int led1_state = 0;      // Whether LED 1 is on or off.
int led2_state = 0;      // Whether LED 2 is on or off.
int led3_state = 0;      // Whether LED 3 is on or off.

void setup()
{
  pinMode (LED1, OUTPUT);	
  pinMode (LED2, OUTPUT);	
  pinMode (LED3, OUTPUT);	
  pinMode (BUTTON, INPUT);
 }

void loop()
{

#if OTHER_LEDS == 1

  toggle (LED1, &led1_state);
  delay(T/2);  

  toggle (LED2, &led2_state);
  delay(T/2); 
#endif

  // Detect button change

  button_before = button_now;
  button_now = digitalRead(BUTTON);
  
  if (button_before == LOW && button_now == HIGH)
  {
    toggle (LED3, &led3_state);
  }
}

void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}