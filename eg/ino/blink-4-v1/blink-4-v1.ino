#include <Arduino_FreeRTOS.h>

#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 
#define LED3    6  // pin of the LED 2 
#define BUTTON  2  // opin of the push button

struct led_t
{
  int pin; 
  int state = 0;
  int period;
} led1, led2;

void setup()
{
  pinMode (LED1, OUTPUT);	
  pinMode (LED2, OUTPUT);	
  pinMode (LED3, OUTPUT);	
  pinMode (BUTTON, INPUT);

  led1.pin = LED1;
  led1.period = 500; 
  led2.pin = LED2;
  led2.period = 500; 


  xTaskCreate(
    toggle,            // Task function
    "Toggle LED 1",    // Desciptive string
    100,               // Stack depth
    &led1,             // Task argument
    1,                 // Task priority
    NULL               // Task handler
  );

  xTaskCreate(
    toggle,            // Task function
    "Toggle LED 2",    // Desciptive string
    100,               // Stack depth
    &led2,             // Task argument
    1,                 // Task priority
    NULL               // Task handler
  );


}

void toggle (void *args)
{
  struct led_t *led;
  led = (struct led_t *) args;
 
  while (1)
  {
    led->state = led->state ? 0 : 1;
    digitalWrite (led->pin, led->state);
    vTaskDelay (pdMS_TO_TICKS(led->period));
    
  }
}


void loop(){}
