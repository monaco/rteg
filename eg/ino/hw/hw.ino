
#define T    1000     // LED blinking period

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);  
}

void loop()
{
  digitalWrite(LED_BUILTIN, HIGH);  
  delay(T/2);                      
  digitalWrite(LED_BUILTIN, LOW);   
  delay(T/2);                      
}

