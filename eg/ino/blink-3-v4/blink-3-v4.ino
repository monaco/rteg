#include "timer-interrupt.h"
#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 
#define LED3    6  // pin of the LED 2 
#define BUTTON  2  // opin of the push button

#define R     40 // Interrupt time resolution (ms)
#define T1    13	  // LED1 period T1 * R = 520ms
#define T2    14    // LED2 period T2 * R = 520ms

#define STEP_MAX 13

volatile int task;
volatile int step;

void setup()
{
  pinMode (LED1, OUTPUT);	 
  pinMode (LED2, OUTPUT);

  timer1_interval(R);  

  step = 0;

  attachInterrupt(digitalPinToInterrupt(BUTTON), led3_toggle, RISING);
}

void task_0(), task_1();
void (*tasks[])()  = {task_0, task_1, NULL};

void loop()
{
}


void timer1_action()
{
  timer1_reset();
  task = 0;
  while (tasks[task])
     tasks[task++]();
  step = (step + 1) % STEP_MAX; 
}
volatile unsigned int led1_count = 0;   
volatile unsigned int led2_count = 0;  

volatile unsigned int led1_state = 0;   
volatile unsigned int led2_state = 0;  
volatile unsigned int led3_state = 0;    

void task_0()
{
   switch (led1_count)               
  {
    case 0:
      toggle (LED1, &led1_state);    // 4 pulsos, 7 intervals, 8 changes : 7 * 40ms = 280ms
      break;
    case 1:
      toggle (LED1, &led1_state);
      break;
    case 2:
      toggle (LED1, &led1_state);
      break;
    case 3:
      toggle (LED1, &led1_state);
      break;
    case 4:
      toggle (LED1, &led1_state);
      break;
    case 5:
      toggle (LED1, &led1_state);
      break;
    case 6:
      toggle (LED1, &led1_state);
      break;
    case 7:
      toggle (LED1, &led1_state);
      break;
    default:                        // MAX_STEP-7 =  6 intervals : 6 * 40ms =  240ms
      break;
  }  
 led1_count = (led1_count + 1) % T1 ;  // Period T1 * R
}

void task_1()
{
  switch (led2_count)
   {
     case 0:   // Skip 3 first ticks to phase-shift task 2.
     case 1:   // 3 intervals, 4 
     case 2:
        break;
     case 3:
        toggle (LED2, &led2_state); // 4 pulsos, 7 intervals, 8 intervals: 7 * 40ms = 280ms
        break;  
     case 4:
        toggle (LED2, &led2_state);
        break;
     case 5:
        toggle (LED2, &led2_state); 
        break;  
     case 6:
        toggle (LED2, &led2_state);
        break;
     case 7:
        toggle (LED2, &led2_state);
        break;
     case 8:
        toggle (LED2, &led2_state); 
        break;  
     case 9:
        toggle (LED2, &led2_state);
        break;
     case 10:
        toggle (LED2, &led2_state);
        break;
     default:                     // MAX_STEP-7 =  6 intervals : 6 * 40ms =  240ms
        break;
   }
 led2_count = (led2_count + 1) % T2;   // Period T2 * R  
}  

void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}

void led3_toggle()
{
  toggle (LED3, &led3_state);
}