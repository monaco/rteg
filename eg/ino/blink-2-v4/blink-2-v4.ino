
#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 

#define T     500	// LED blinking period

int led1_state = 0;  // LED 1 starts off
int led2_state = 0;  // LED 2 starts on (phased T/4)

void setup()
{
  pinMode (LED1, OUTPUT);	 
  pinMode (LED2, OUTPUT);	
}

void loop()
{ 
  toggle (LED1, &led1_state);

  delay(T*2/5);
  
  toggle (LED2, &led2_state);

  delay(T*3/5);

  toggle (LED1, &led1_state);

  delay(T*3/5);

  toggle (LED2, &led2_state);

  delay(T*2/5);
}


void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}