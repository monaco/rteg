#include <Arduino_FreeRTOS.h>
#include <event_groups.h>

#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 
#define LED3    6  // pin of the LED 2 
#define BUTTON  2  // opin of the push button

struct led_t
{
  int pin; 
  int state = 0;
  int period;
} led1, led2;

EventGroupHandle_t barrier;
// #define TASK1_BIT  (1 << 0)
// #define TASK2_BIT  (1 << 1) 

void setup()
{
  pinMode (LED1, OUTPUT);	
  pinMode (LED2, OUTPUT);	
  pinMode (LED3, OUTPUT);	
  pinMode (BUTTON, INPUT);

  led1.pin = LED1;
  led1.period = 100; 
  led2.pin = LED2;
  led2.period = 50;  

  barrier = xEventGroupCreate();
  //xEventGroupClearBits(barrier, TASK1_BIT | TASK2_BIT);
  xEventGroupClearBits(barrier, B11);


  xTaskCreate(
    toggle,            // Task function
    "Toggle LED 1",    // Desciptive string
    100,               // Stack depth
    &led1,             // Task argument
    1,                 // Task priority
    NULL               // Task handler
  );

  xTaskCreate(
    toggle,            // Task function
    "Toggle LED 2",    // Desciptive string
    100,               // Stack depth
    &led2,             // Task argument
    1,                 // Task priority
    NULL               // Task handler
  );


}

void toggle (void *args)
{
  struct led_t *led;
  led = (struct led_t *) args;

   if (led->pin == LED2)             // Task2's initialization latency.          
    vTaskDelay(pdMS_TO_TICKS(20));

  switch(led->pin)
   {
    case LED1:   // Wait for task 2 to complete initalization.
        // xEventGroupSetBits(barrier, TASK1_BIT);
        // xEventGroupWaitBits(barrier, TASK2_BIT, pdTRUE, pdTRUE, portMAX_DELAY);
        xEventGroupSetBits(barrier, B01);
        xEventGroupWaitBits(barrier, B10, pdTRUE, pdTRUE, portMAX_DELAY);
       break;
   case LED2:    // Wait for task 1 to complete initalization
        // xEventGroupSetBits(barrier, TASK2_BIT);
        // xEventGroupWaitBits(barrier, TASK1_BIT, pdTRUE, pdTRUE, portMAX_DELAY);
        xEventGroupSetBits(barrier, B10);
        xEventGroupWaitBits(barrier, B01, pdTRUE, pdTRUE, portMAX_DELAY);
       break;
  }
 
  while (1)
  {
    led->state = led->state ? 0 : 1;
    digitalWrite (led->pin, led->state);
    vTaskDelay (pdMS_TO_TICKS(led->period));
    
  }
}


void loop(){}
