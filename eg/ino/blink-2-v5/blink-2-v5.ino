
#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 

#define T     500	// LED blinking period

void setup()
{
  pinMode (LED1, OUTPUT);	 
  pinMode (LED2, OUTPUT);	
}

enum {task_0, task_1} task = task_0;
enum {step_0, step_1} step = step_0;

void loop()
{ 

  switch(task)
  {

    case task_0:                                 // task 0
      switch (step)
      {
        case step_0:                             // task 0, step 0
          digitalWrite (LED1, HIGH);
          delay(T*2/5);
          task = task + 1;
          break;

        case step_1:                             // task 0, step 1
          digitalWrite (LED1, LOW);
          delay(T*3/5);
          task = task + 1;
          break;
     }  
      break;

    case task_1:                                 // task 1
      switch (step)
      {
        case step_0:                             // task 1, step 0
          digitalWrite (LED2, HIGH);
          delay(T*3/5);
          task = task + 1;
          break;  

        case step_1:                             // task 1, step 1
          digitalWrite (LED2, LOW);
          delay(T*2/5);
          task = task + 1;
          break;
      }
      break;

    default:                                     // no more tasks
      task = 0;
      step = (step + 1) % 2;
      break;
    }
}

