
#define LED1    8  // Pin of the LED 1
#define LED2    7  // pin of the LED 2 

#define T     500	// LED toogling period

int led1_state = 0;  // LED 1 starts off
int led2_state = 1;  // LED 2 phase-shifted of T

void setup()
{
  pinMode (LED1, OUTPUT);	
  pinMode (LED2, OUTPUT);	
}

void loop()
{
  toggle (LED1, &led1_state);

  toggle (LED2, &led2_state);

  delay(T); 
}

void toggle (int led, int *state)
{
  *state = *state ? 0 : 1;
  digitalWrite (led, *state);
}